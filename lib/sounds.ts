import { useSound } from 'use-sound';
import { usePersistantState } from '.';

export function useClick() {
	const state = usePersistantState();
	const result = useSound('/sounds/clicking.ogg', {
		volume: 0.02,
	});

	if (!state.get().sound) return [() => {}, null];

	return result;
}
