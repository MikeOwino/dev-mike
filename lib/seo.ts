import { NextSeo } from 'next-seo';
import { useRouter } from 'next/router';

import type { ComponentProps } from 'react';

export function useSeoProps(
	props: Partial<ComponentProps<typeof NextSeo>> = {},
): Partial<ComponentProps<typeof NextSeo>> {
	const router = useRouter();

	const title = 'Mike Owino | Blockchain Developer';
	const description = "A blockchain developer BUIDLING on Ethereum and Binance Smart Chain";

	return {
		title,
		description,
		canonical: `https://mikeowino.com/${router.asPath}`,
		openGraph: {
			title,
			description,
			site_name: 'mikeowino',
			url: `https://mikeowino.com/${router.asPath}`,
			type: 'website',
			images: [
				{
					url: '/banner.png',
					alt: description,
					width: 1280,
					height: 720,
				},
			],
		},
		twitter: {
			cardType: 'summary_large_image',
			handle: '@mikeyhodl',
			site: '@mikeyhodl',
		},
		...props,
	};
}
